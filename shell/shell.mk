INSTALLERS += shell
CLEANERS += clean_shell

SHELL_SRC_DIR := $(DOTFILES)/shell
SHELL_DST_DIR := $(DST_DIR)/.shell
BASH_SRC_DIR := $(SHELL_SRC_DIR)/bash
BASH_DIR := $(DST_DIR)/.bash
BASH_PROFILE := $(DST_DIR)/.bash_profile
BASHRC := $(DST_DIR)/.bashrc
BASH_COMPLETION := $(DST_DIR)/.bash_completion

.PHONY: shell clean_shell

shell: banner_install_shell $(SHELL_DST_DIR) $(BASH_DIR) $(BASHRC) $(BASH_COMPLETION)

$(SHELL_DST_DIR):
	$(LINK) $(SHELL_SRC_DIR) $@

$(BASH_DIR):
	$(LINK) $(BASH_SRC_DIR)/bash $@

$(BASHRC):
	$(LINK) $(BASH_SRC_DIR)/bashrc $@

$(BASH_COMPLETION):
	$(LINK) $(BASH_SRC_DIR)/bash_completion $@

clean_shell: banner_clean_shell
	$(RM) $(SHELL_DST_DIR)
	$(RM) $(BASH_DIR)
	$(RM) $(BASHRC)
	$(RM) $(BASH_PROFILE)
